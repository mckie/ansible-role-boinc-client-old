# Boinc client

Install and configure the BOINC client.

## Requirements

Python's `netaddr` needs to be installed on the controller applying this role:

```shell
pip install netaddr

# package managers
dnf install python3-netaddr
pacman install python-netaddr
```

## Role Variables

Variable                         | Type       | Default value               | Description
---------------------------------|------------|-----------------------------|------------
`acct_mgr_attach`                | boolean    | `false`                     | Attach to an account manager; if `true` will require the other `acct_mgr_*` variables
`acct_mgr_password`              | string     | null                        | Password for the account manager connection
`acct_mgr_url`                   | URL        | <http://bam.boincstats.com> | URL of the account manager
`acct_mgr_username`              | string     | null                        | Username for the account manager connection
`boinc_group`                    | string     | boinc                       | Owner of the files of the client
`boinc_user`                     | string     | boinc                       | Owner of the files of the client
`boinc_config_dir`               | string     | dependent on the os' family | BOINC's configuration directory; defaults to `boinc_data_dir`'s value, but some Linux distribution use `/etc/boinc-client`
`boinc_config_dir_by_os_family`  | dictionary | -                           | Default paths to BOINC's configuration directory by OS family
`boinc_data_dir`                 | string     | dependent on the os         | BOINC's data directory; see [BOINC Data directory] for defaults; defaults to `/var/lib/boinc` on Linux
`boinc_data_dir_by_system`       | dictionary | -                           | Default paths to BOINC's data directory by system; see see [BOINC Data directory]
`gui_rpc_auth_password`          | string     | null                        | Password for the GUI; if not set, the client will create a new one on start
`headless`                       | boolean    | `true`                      | If `true`, try to make the client as headless as possible
`installation_method`            | string     | auto                        | Installation method; needs to be one of those listed in `supported_installation_methods`
`packages_by_pkg_mgr`            | dictionary | -                           | List of packages to install, by package manager
`remote_access_enabled`          | boolean    | `false`                     | Enable remote access from the BOINC manager application
`remote_hosts`                   | list       | []                          | List of the allowed hosts for remote access; entries need to be valid IPv4 addresses
`set_group_permissions`          | boolean    | `false`                     | Set group permissions where allowed
`start_at_boot`                  | boolean    | `false`                     | Make the client start at boot
`supported_installation_methods` | list       | -                           | `auto` = recommended method from [Installing BOINC]
`supported_systems`              | list       | -                           | List of the systems this role supports

## Dependencies

None.

## Example Playbook

```yaml
- hosts: managed_clients
  roles:
    - role: boinc_client
      tags:
        - boinc
        - client
      vars:
        acct_mgr_attach: true
        acct_mgr_username: jbgood
        acct_mgr_password: b3st-password-evah!
        gui_rpc_auth_password: "{{ lookup('env', 'GUI_RPC_AUTH_PASSWORD') }}"
        headless: true
        remote_access_enabled: true
        remote_hosts:
          - '192.168.100.150'
        set_group_permissions: true
```

## License

MIT

## Sources

- [Installing BOINC]
- [BOINC][arch wiki boinc] page on the [Arch Wiki]
- [BOINC Data directory]

[arch wiki]: https://wiki.archlinux.org

[arch wiki boinc]: https://wiki.archlinux.org/?title=BOINC
[boinc data directory]: https://boinc.berkeley.edu/wiki/BOINC_Data_directory
[installing boinc]: https://boinc.berkeley.edu/wiki/Installing_BOINC
